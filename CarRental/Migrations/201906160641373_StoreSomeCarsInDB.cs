namespace CarRental.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StoreSomeCarsInDB : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Cars (Made,BookTime,Booked) VALUES ('Toyota',null,0)");
            Sql("INSERT INTO Cars (Made,BookTime,Booked) VALUES ('Mazda',null,0)");
            Sql("INSERT INTO Cars (Made,BookTime,Booked) VALUES ('Volkswagon',null,0)");
            Sql("INSERT INTO Cars (Made,BookTime,Booked) VALUES ('Honda',null,0)");
            Sql("INSERT INTO Cars (Made,BookTime,Booked) VALUES ('BMW',null,0)");
            Sql("INSERT INTO Cars (Made,BookTime,Booked) VALUES ('Audi',null,0)");
        }

        public override void Down()
        {
        }
    }
}
