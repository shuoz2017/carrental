﻿using AutoMapper;
using CarRental.CarListRepository;
using CarRental.DTOs;
using CarRental.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;

namespace CarRental.Controllers.API
{
    public class CarRentalController : Controller
    {
        private readonly ICarListRepository _carListRepository;
        public CarRentalController(ICarListRepository carListRepository)
        {
            _carListRepository = carListRepository;
        }

        [System.Web.Mvc.AcceptVerbs(HttpVerbs.Get | HttpVerbs.Options)]
        public async Task<JsonResult> GetCarList()
        {
            var carList = await _carListRepository.GetCarListAsync();

            if (carList.Count == 0)
                return Json("Sorry, All Cars Are Booked", JsonRequestBehavior.AllowGet);

            var catListDto = new List<CarDto>();
            carList.ForEach(p =>
            {
                var carDto = Mapper.Map<Car, CarDto>(p);
                catListDto.Add(carDto);
            });

            return Json(catListDto, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Mvc.AcceptVerbs(HttpVerbs.Put | HttpVerbs.Options)]
        public async Task<JsonResult> BookCar([FromBody]int id)
        {
            var car = await _carListRepository.BookCarAsync(id);
            if (car == null)
                return Json("Sorry, Car not found or been booked");
            return Json(Mapper.Map<Car, CarDto>(car));
        }
    }
}