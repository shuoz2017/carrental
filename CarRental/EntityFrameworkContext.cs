﻿using CarRental.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CarRental
{
    public class EntityFrameworkContext : DbContext
    {
        public EntityFrameworkContext()
        {
            Database.SetInitializer<EntityFrameworkContext>(null);
        }
        public DbSet<Car> Cars { get; set; }

    }
}