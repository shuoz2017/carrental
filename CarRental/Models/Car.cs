﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarRental.Models
{
    public class Car
    {
        public int ID { get; set; }

        [Required]
        [MaxLength(25)]
        public string Made { get; set; }

        public DateTime? BookTime { get; set; }

        public bool Booked { get; set; }

    }
}