﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarRental.DTOs
{
    public class CarDto
    {
        public int ID { get; set; }

        public string Made { get; set; }

        //public DateTime? BookTime { get; set; }

        public bool Booked { get; set; }


    }
}