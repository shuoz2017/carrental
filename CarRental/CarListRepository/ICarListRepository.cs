﻿using CarRental.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRental.CarListRepository
{
    public interface ICarListRepository
    {
        Task<List<Car>> GetCarListAsync();
        Task<Car> BookCarAsync(int ID);
        Task<bool> BookCarListAsync(IEnumerable<int> BookCarIDList);
    }
}
