﻿using CarRental.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace CarRental.CarListRepository
{
    public class CarListRepository : ICarListRepository
    {
        public async Task<List<Car>> GetCarListAsync()
        {
            var result = new List<Car>();
            using (var db = new EntityFrameworkContext())
            {
                result = await db.Cars.Where(p => p.BookTime == null || DbFunctions.AddHours(p.BookTime, 24) < DateTime.Now)
                    .ToListAsync().ConfigureAwait(false);

                if (result.Count != 0)
                {
                    result.ForEach(car =>
                    {
                        car.BookTime = null;
                        car.Booked = false;
                    });
                    await db.SaveChangesAsync();
                }
            }
            return result;
        }

        public async Task<Car> BookCarAsync(int ID)
        {
            Car car = null;
            using (var db = new EntityFrameworkContext())
            {
                car = await db.Cars.SingleOrDefaultAsync(p => p.ID == ID && p.Booked == false).ConfigureAwait(false);
                if (car != null)
                {
                    car.BookTime = DateTime.Now;
                    car.Booked = true;
                    await db.SaveChangesAsync();
                }
            }
            return car;
        }

        public async Task<bool> BookCarListAsync(IEnumerable<int> BookCarIDList)
        {
            foreach (var car in BookCarIDList)
            {
                await BookCarAsync(car);
            }
            return true;
        }

    }
}